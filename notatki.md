## GIT

cd ..
git clone https://bitbucket.org/ev45ive/react-open-paz.git react-open-paz
cd react-open-paz
npm i
npm start

### Git Update

git stash -u
git pull

<!-- lub -->

git pull -u origin master

## Instalacje

https://nodejs.org/en/

node -v
v14.17.0

npm --version
6.14.6

## Node 17 ssl problem

export NODE_OPTIONS=--openssl-legacy-provider

code -v
1.61.2
6cba118ac49a1b88332f312a8f67186f7f3c1643
x64

git --version
git version 2.31.1.windows.1

Google Chrome
chrome://version
95.0.4638.54

## Rozszerzenia

https://marketplace.visualstudio.com/items?itemName=dsznajder.es7-react-js-snippets
https://chrome.google.com/webstore/detail/react-developer-tools/fmkadmapgofadopljbjfkapdkoienihi
https://marketplace.visualstudio.com/items?itemName=quicktype.quicktype

## Create REact app

https://create-react-app.dev/

npx create-react-app --help
mkdir -p C:/projects/szkolenia/sages-react-open-paz
cd C:/projects/szkolenia/sages-react-open-paz

npx create-react-app . --template=typescript

npm start

## Bootstrap CSS

npm install bootstrap

## Playlists app

mkdir -p src/playlists/containers
touch src/playlists/containers/PlaylistsView.tsx

mkdir -p src/playlists/components
touch src/playlists/components/PlaylistList.tsx
touch src/playlists/components/PlaylistDetails.tsx
touch src/playlists/components/PlaylistEditor.tsx

## Music search module

mkdir -p src/core/model
touch src/core/model/Album.ts

mkdir -p src/music/containers
touch src/music/containers/AlbumSearchView.tsx

mkdir -p src/music/components
touch src/music/components/SearchForm.tsx
touch src/music/components/ResultsGrid.tsx
touch src/music/components/AlbumCard.tsx

## HTTP
https://github.com/axios/axios#installing
https://swr.vercel.app/docs/getting-started
https://use-http.com/#/

mkdir -p src/core/services
touch src/core/services/SearchService.ts 

## Oauth

<!-- https://www.npmjs.com/package/immutable -->

npm i react-oauth2-hook prop-types immutable react-storage-hook


## Redux

https://redux.js.org/
https://react-redux.js.org/
https://github.com/reduxjs/reselect
https://immerjs.github.io/immer/

// => 
https://redux-toolkit.js.org/introduction/getting-started

https://chrome.google.com/webstore/detail/redux-devtools/lmhkpmbekcpmknklioeibfkpmmfibljd


## Testing

https://jestjs.io/docs/bypassing-module-mocks
https://mswjs.io/

https://testing-library.com/docs/queries/about#types-of-queries