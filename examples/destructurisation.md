```js

person = {
    name:'alice', //age:21,
    address:{ street:'sezamkowa' }, 
    company:{ name:'ACME' }, 
    score:[100,200,300]
}

var { name: personName } = person;
{name: 'alice', address: {…}, company: {…}, score: Array(3)}
personName
'alice'
person = {
    name:'alice', //age:21,
    address:{ street:'sezamkowa' }, 
    company:{ name:'ACME' }, 
    score:[100,200,300]
}

var { 
    name: personName, 
    address:{ street: personAddress },
    score: [ lastScore, prevScore] 
 } = person;
// {name: 'alice', address: {…}, company: {…}, score: Array(3)}

personName
// 'alice'
personAddress
// 'sezamkowa'
lastScore
// 100
prevScore
// 200

```

```js
var person = {
    name:'alice', //age:21,
    address:{ street:'sezamkowa' }, 
//     company:{ name:'ACME' },
    score:[100,200,300]
}

// var getInfo = (person) => {
//     var {
//         name: personName,
//         address:{ street: personAddress },
//         score: [ lastScore, prevScore]
//      } = person;
// return `${personName} lives on ${personAddress} and scored ${lastScore}`
// }

var getInfo = ({ 
//     name: name,
    name,
    company: { name: companyName } = {name:'not employed'},
    address:{ street: personAddress },
    score: [ lastScore, prevScore] 
 }) => `${name} (${companyName}) lives on ${personAddress} and scored ${lastScore}`;

getInfo(person) 
'alice (not employed) lives on sezamkowa and scored 100'
```