import { Playlist } from "../../core/model/Playlist";

export const playlistsMock: Playlist[] = [
  {
    id: "123",
    name: "Playlist 123",
    public: true,
    description: "my 123 playlist",
  },
  {
    id: "234",
    name: "Playlist 234",
    public: false,
    description: "my 234 playlist",
  },
  {
    id: "345",
    name: "Playlist 345",
    public: true,
    description: "my 345 playlist",
  },
];
