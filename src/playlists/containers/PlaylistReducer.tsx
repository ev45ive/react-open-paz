import { Reducer } from "@reduxjs/toolkit";
import axios from "axios";
import { PagingObject } from "../../core/model/Album";
import { Playlist } from "../../core/model/Playlist";
import { playlistsMock } from "./playlistsMock";

export const PlalistReducer = {};
interface PlaylistsState {
  items: Playlist[];
  selectedId?: Playlist["id"];
  mode: "details" | "editor" | "create";
  loading: boolean;
  message: string;
}
export const initialState: PlaylistsState = {
  items: [],
  selectedId: undefined,
  mode: "details",
  loading: false,
  message: "",
};
type Actions = ReturnType<
  | typeof playlistSelect
  | typeof playlistRemove
  | typeof playlistMode
  | typeof playlistUpdate
  | typeof playlistCreate
  | typeof playlistLoadSuccess
  | typeof playlistLoadStart
  | typeof playlistLoadFailed
>;

/* === REDUCER */
export const reducer: Reducer<PlaylistsState, Actions> = (
  state = initialState,
  action
) => {
  console.log(state, action);

  switch (action.type) {
    case "PLAYLIST_LOAD_START":
      return { ...state, items: [], loading: true };
    case "PLAYLIST_LOAD_SUCCESS":
      return { ...state, items: action.payload.data, loading: false };
    case "PLAYLIST_LOAD_FAILED":
      return { ...state, loading: false, message: action.error?.message };
    case "PLAYLIST_SELECT":
      return { ...state, selectedId: action.payload.id };
    case "PLAYLIST_MODE":
      return { ...state, mode: action.payload.mode };
    case "PLAYLIST_UPDATE": {
      const draft = action.payload.draft;
      return {
        ...state,
        items: state.items.map((p) => (p.id === draft.id ? draft : p)),
        mode: "details",
        selectedId: draft.id,
      };
    }
    case "PLAYLIST_CREATE": {
      const draft = action.payload.draft;
      return {
        ...state,
        items: [...state.items, draft],
        mode: "details",
        selectedId: draft.id,
      };
    }
    case "PLAYLIST_REMOVE":
      return {
        ...state,
        items: state.items.filter((p) => p.id !== action.payload.id),
      };
    default:
      const _youForgotAnActionDummy: never = action; // exhaustiveness check!
      return state;
  }
};

/* === ACTIONS */
export const playlistLoadStart = () =>
  ({ type: "PLAYLIST_LOAD_START" } as const);

export const playlistLoadFailed = (error: Error) =>
  ({ type: "PLAYLIST_LOAD_FAILED", error } as const);

export const playlistLoadSuccess = (data: Playlist[]) =>
  ({
    type: "PLAYLIST_LOAD_SUCCESS",
    payload: { data },
  } as const);

export const playlistSelect = (id: Playlist["id"]) =>
  ({
    type: "PLAYLIST_SELECT",
    payload: { id },
  } as const);

export const playlistRemove = (id: Playlist["id"]) =>
  ({
    type: "PLAYLIST_REMOVE",
    payload: { id },
  } as const);

export const playlistMode = (mode: PlaylistsState["mode"]) =>
  ({
    type: "PLAYLIST_MODE",
    payload: { mode },
  } as const);
export const playlistUpdate = (draft: Playlist) =>
  ({
    type: "PLAYLIST_UPDATE",
    payload: { draft },
  } as const);

export const playlistCreate = (draft: Playlist) => {
  draft.id = Date.now().toString();
  return {
    type: "PLAYLIST_CREATE",
    payload: { draft },
  } as const;
};

export const asyncLoadPlaylists = (dispatch: React.Dispatch<Actions>) => {
  // dispatch(playlistLoadSuccess(playlistsMock))
  dispatch(playlistLoadStart());
  return axios
    .get<PagingObject<Playlist>>("https://api.spotify.com/v1/me/playlists")
    .then((res) => res.data.items)
    .then((data) => dispatch(playlistLoadSuccess(data)))
    .catch((error) => dispatch(playlistLoadFailed(error)));
};

export const makeSelectors = (dispatch: React.Dispatch<Actions>) => {
  return {
    selectPlaylistById: (id: string) => dispatch(playlistSelect(id)),
    removePlaylistById: (id: string) => dispatch(playlistRemove(id)),
    createMode: () => dispatch(playlistMode("create")),
    cancel: () => dispatch(playlistMode("details")),
    edit: () => dispatch(playlistMode("editor")),
    saveNewPlaylist: (draft: Playlist) => dispatch(playlistCreate(draft)),
    saveChangedPlaylist: (draft: Playlist) => dispatch(playlistUpdate(draft)),
  };
};

/* === SELECTORS */

// export const selectCurrentPlaylist = (state: PlaylistsState) =>
//   state.items.find((p) => p.id == state.selectedId);

// https://github.com/reduxjs/reselect
