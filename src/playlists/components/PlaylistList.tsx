import React from "react";
import { Playlist } from "../../core/model/Playlist";
import { cls } from "./utils";

interface Props {
  playlists: Playlist[];
  selectedId?: Playlist["id"];
  onSelect: (id: Playlist["id"]) => void;
  onRemove: (id: Playlist["id"]) => void;
}

export const PlaylistList = React.memo(
  ({ playlists, selectedId, onSelect, onRemove }: Props) => {
    console.log("render list");

    return (
      <div>
        <div className="list-group">
          {playlists.map((item, index) => (
            <div
              className={
                // "list-group-item" + (selectedId === item.id ? " active" : "")
                cls("list-group-item", selectedId === item.id && "active")
              }
              key={item.id}
              onClick={() => onSelect(item.id)}
            >
              {index + 1}. {item.name}
              <span
                className="close float-end"
                onClick={(event) => {
                  event.stopPropagation();
                  onRemove(item.id);
                }}
              >
                &times;
              </span>
            </div>
          ))}
        </div>
      </div>
    );
  }
  // ,(prevProps: Readonly<Props>, nextProps: Readonly<Props>) => true
);
