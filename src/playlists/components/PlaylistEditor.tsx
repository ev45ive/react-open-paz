import React, {
  useEffect,
  useMemo,
  useRef,
  useState,
} from "react";
import { Playlist } from "../../core/model/Playlist";

interface Props {
  playlist?: Playlist;
  onCancel: () => void;
  onSave: (draft: Playlist) => void;
}

export const PlaylistEditor = React.memo(
  ({
    playlist = {
      id: "",
      name: "",
      public: false,
      description: "",
    },
    onCancel,
    onSave,
  }: Props) => {
    const [playlistName, setPlaylistName] = useState(playlist.name);
    const [isPublic, setIsPublic] = useState(playlist.public);
    const [description, setDescription] = useState(playlist.description);
    const [message, setMessage] = useState("");

    useEffect(() => {
      setPlaylistName(playlist.name);
      setIsPublic(playlist.public);
      setDescription(playlist.description);
      nameInputRef.current?.focus();
    }, [playlist.id]);

    const saveDraft = () => {
      const draft = {
        ...playlist,
        // id: playlist.id,
        name: playlistName,
        public: isPublic,
        description,
      };
      onSave(draft);
    };

    const nameInputRef = useRef<HTMLInputElement>(null);
    useEffect(() => {
      nameInputRef.current?.focus();
      // document.getElementById('playlist_name')?.focus()
    }, []);

    // Warning: Can't perform a React state update on an unmounted component.
    // This is a no-op, but it indicates a memory leak in your application.
    // To fix, cancel all subscriptions and asynchronous tasks in a useEffect cleanup function.
    useEffect(() => {
      if (playlistName === playlist.name) return;
      let handle2: NodeJS.Timeout;

      const handle = setTimeout(() => {
        setMessage("Hello " + playlistName);
        handle2 = setTimeout(() => setMessage(""), 2000);
      }, 400);

      return () => {
        clearTimeout(handle);
        clearTimeout(handle2);
      }; // <- useEffect cleanup function
    }, [playlistName]);

    return (
      <div>
        {message && <p className="alert alert-info mb-3">{message}</p>}

        {useMemo(
          () => (
            <div className="form-group mb-3">
              <label htmlFor="playlist_name">Name:</label>
              <input
                id="playlist_name"
                ref={nameInputRef}
                type="text"
                className="form-control"
                value={playlistName}
                onChange={(e) => setPlaylistName(e.target.value)}
              />
              <span data-testid="counter">{playlistName.length} / 170</span>
            </div>
          ),
          [playlistName]
        )}

        <div className="form-group mb-3">
          <label>
            <input
              type="checkbox"
              checked={isPublic}
              onChange={(e) => setIsPublic(e.currentTarget.checked)}
            />
            Public
          </label>
        </div>

        <div className="form-group mb-3">
          <label>Description:</label>
          <textarea
            className="form-control"
            value={description}
            onChange={(e) => setDescription(e.currentTarget.value)}
          />
        </div>

        <button className="btn btn-danger" onClick={onCancel}>
          Cancel
        </button>
        <button className="btn btn-success" onClick={saveDraft}>
          Save
        </button>
      </div>
    );
  }
);
