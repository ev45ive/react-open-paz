import React from "react";
import { render, screen } from "@testing-library/react";
import { PlaylistDetails } from "./PlaylistDetails";
import { Playlist } from "../../core/model/Playlist";
import { playlistsMock } from "../containers/playlistsMock";
import userEvent from "@testing-library/user-event";

const setup = ({
  onEdit = jest.fn(),
  playlist,
}: {
  playlist?: Playlist;
  onEdit?: () => void;
}) => {
  render(<PlaylistDetails onEdit={onEdit} playlist={playlist} />);

  return { onEdit };
};

test("renders without playlist", () => {
  setup({});
  const linkElement = screen.getByText(/No playlist selected/i);
  expect(linkElement).toBeInTheDocument();
});

test("renders playlist details", () => {
  setup({ playlist: playlistsMock[0] });
  const linkElement = screen.getByText(playlistsMock[0].name);
  expect(linkElement).toBeInTheDocument();
});

test("clicking edit emits event", () => {
  const { onEdit } = setup({ playlist: playlistsMock[0] });
  const linkElement = screen.getByRole("button", { name: "Edit" });

  userEvent.click(linkElement)

  expect(onEdit).toHaveBeenCalledTimes(1)
});
