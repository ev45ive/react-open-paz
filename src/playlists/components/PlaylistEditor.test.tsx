import React from "react";
import { render, screen } from "@testing-library/react";
import { PlaylistDetails } from "./PlaylistDetails";
import { Playlist } from "../../core/model/Playlist";
import { PlaylistEditor } from "./PlaylistEditor";
import { playlistsMock } from "../containers/playlistsMock";
import userEvent from "@testing-library/user-event";

const setup = ({
  onEdit = jest.fn(),
  onSave = jest.fn(),
  playlist,
}: {
  playlist?: Playlist;
  onEdit?: () => void;
  onSave?: (draft: Playlist) => void;
}) => {
  render(
    <PlaylistEditor onCancel={onEdit} onSave={onSave} playlist={playlist} />
  );

  return { onEdit, onSave, playlist };
};

test("renders without playlist", () => {
  setup({ playlist: undefined });
  const linkElement = screen.getByLabelText("Name:");
  expect(linkElement).toBeInTheDocument();
  expect(linkElement).toHaveValue("");
});

test("renders  playlist", () => {
  setup({ playlist: playlistsMock[0] });
  const linkElement = screen.getByLabelText("Name:");
  expect(linkElement).toBeInTheDocument();
  expect(linkElement).toHaveValue(playlistsMock[0].name);
});

test("renders playlist length counter", () => {
  setup({ playlist: playlistsMock[0] });
  const linkElement = screen.getByLabelText("Name:");
  userEvent.clear(linkElement);
  userEvent.type(linkElement, "Ala ma kota");
  const counterElement = screen.getByTestId("counter");
  expect(counterElement).toHaveTextContent("11 / 170");
});
