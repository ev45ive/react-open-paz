import { AlbumSearchView } from "./music/containers/AlbumSearchView";
import { Redirect, Route, Switch } from "react-router-dom";
import { NavBar } from "./core/components/NavBar";
import { AlbumDetailsView } from "./music/containers/AlbumDetailsView";
import { PlaylistsReducerView } from "./playlists/containers/PlaylistsReducerView";

function App() {
  return (
    <div className="App">
      <NavBar></NavBar>


      <div className="container">
        <div className="row">
          <div className="col">
            <Switch>
              <Redirect path="/" exact={true} to="/playlists" />
              <Route path="/playlists" component={PlaylistsReducerView} />
              <Route path="/music/search" component={AlbumSearchView} />
              <Route
                path="/music/albums/:albumId"
                component={AlbumDetailsView}
              />
              <Route
                path="*"
                render={() => (
                  <h1 className="text-center mt-5 bg-black text-white p-5">
                    Page not found
                  </h1>
                )}
              />
            </Switch>
          </div>
        </div>
      </div>
    </div>
  );
}

export default App;
