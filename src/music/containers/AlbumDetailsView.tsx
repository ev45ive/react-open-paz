import React, { useEffect, useState } from "react";
import { useLocation, useParams } from "react-router";
import { useFetchAlbum } from "../../core/hooks/useFetchAlbum";
import { AlbumCard } from "../components/AlbumCard";
import { TrackList } from "../components/TrackList";

interface Props {}

export const AlbumDetailsView = (props: Props) => {
  const { albumId } = useParams<{ albumId: string }>();
  const { data: album, error, loading } = useFetchAlbum(albumId);

  if (loading) return <p className="alert alert-info">Loading</p>;
  if (error) return <p className="alert alert-info">{error.message}</p>;

  return (
    (album && (
      <div>
        <div className="row">
          <div className="col">
            <h1>{album?.name}</h1>
            <small className="text-muted">{albumId}</small>
            <hr />
          </div>
        </div>
        <div className="row">
          <div className="col">
            <AlbumCard album={album}></AlbumCard>
            <dl>
              <dt>name</dt>
              <dd>{album.name}</dd>

              <dt>release_date</dt>
              <dd>{album.release_date} </dd>

              <dt>total_tracks</dt>
              <dd>{album.total_tracks}</dd>
            </dl>
          </div>
          <div className="col">
            {album.tracks && <TrackList tracks={album.tracks.items} />}
          </div>
        </div>
      </div>
    )) ||
    null
  );
};
