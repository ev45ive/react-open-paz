import axios, { AxiosError } from "axios";
import React, { useCallback, useEffect, useState } from "react";
import { useOAuth2Token, OAuthCallback } from "react-oauth2-hook";
import { SWRHook, Fetcher, SWRConfiguration, SWRConfig, Key } from "swr";
import { UserProfile } from "../model/UserProfile";
import { SpotifyErrorResponse, userContext } from "./UserContext";

export const UserContextProvider: React.FC = ({ children }) => {
  const [token, getToken] = useOAuth2Token({
    authorizeUrl: "https://accounts.spotify.com/authorize",
    scope: [""],
    clientID: "70e033d77ca542c7a9ef3718e8f636d4",
    redirectUri: document.location.origin + "/index.html",
  });

  useEffect(() => {
    // https://developer.spotify.com/documentation/general/guides/authorization/
    axios.interceptors.response.use(
      (resp) => resp,
      (error: Error | AxiosError<SpotifyErrorResponse | {}>) => {
        if (!("isAxiosError" in error)) {
          throw new Error("Unexpected error");
        }

        if (!error.response) {
          throw new Error("Cannot connect to server");
        }

        if (!("error" in error.response!.data)) {
          throw new Error("Unexpected server error");
        }

        if (error.response?.data.error.status === 401) {
          getToken();
        }

        throw new Error(error.response!.data.error.message);
      }
    );

    const handler = axios.interceptors.request.use((config) => {
      if (!token) {
        throw new Error("Not logged in.");
      }
      config.headers!["Authorization"] = `Bearer ${token}`;
      return config;
    });

    return () => axios.interceptors.request.eject(handler);
  }, [token, getToken]);

  const [user, setUser] = useState<UserProfile | null>(null);

  const login = useCallback(() => {
    // setUser({ display_name: "Placki" } as unknown as UserProfile);
    if (!token) getToken();

    axios.get<UserProfile>(`https://api.spotify.com/v1/me`).then((resp) => {
      setUser(resp.data);
    });
  }, []);

  useEffect(() => {
    if (token) login();
  }, [token]);

  const logout = useCallback(() => {
    setUser(null);
    getToken();
  }, []);

  if (window.location.hash.includes("access_token")) return <OAuthCallback />;
  return (
    <userContext.Provider value={{ login, logout, user }}>
      <SWRConfig
        value={{
          revalidateIfStale: false,
          revalidateOnFocus: false,
          revalidateOnReconnect: false,
          use: [myMiddleware],
          // refreshInterval: 3000,
          fetcher: async (resource, config) => {
            try {
              config = config || { headers: {} };
              config.headers["Authorization"] = `Bearer ${token}`;
              const resp = await axios(resource, config).then(
                (resp) => resp.data
              );
              return resp;
            } catch (err) {
              const error = err as
                | Error
                | AxiosError<SpotifyErrorResponse | {}>;

              if (!("isAxiosError" in error)) {
                throw new Error("Unexpected error");
              }

              if (!error.response) {
                throw new Error("Cannot connect to server");
              }

              if (!("error" in error.response!.data)) {
                throw new Error("Unexpected server error");
              }

              if (error.response?.data.error.status === 401) {
                getToken();
              }

              throw new Error(error.response!.data.error.message);
            }
          },
        }}
      >
        {children}
      </SWRConfig>
    </userContext.Provider>
  );
};
function myMiddleware(useSWRNext: SWRHook) {
  return (
    key: Key,
    fetcher: Fetcher<any> | null,
    config: SWRConfiguration<any, any>
  ) => {
    // Before hook runs...
    // Handle the next middleware, or the `useSWR` hook if this is the last one.
    const swr = useSWRNext(key, fetcher, config);

    return swr;
  };
}
