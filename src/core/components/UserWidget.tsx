import React, { useContext } from "react";
import { userContext } from "../contexts/UserContext";

export const UserWidget = () => {
  const { user, login, logout } = useContext(userContext);

  return (
    <div>
      <div className="navbar-text">
        {!user ? (
          <div>
            Welcome, Guest <span onClick={login}> | Login</span>
          </div>
        ) : (
          <div>
            Welcome, {user.display_name}
            <span onClick={logout}> | Logout</span>
          </div>
        )}
      </div>
    </div>
  );
};


