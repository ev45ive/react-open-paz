import React, { useState } from "react";
import { NavLink } from "react-router-dom";
import { cls } from "../../playlists/components/utils";
import { UserWidget } from "./UserWidget";

interface Props {
  children?: React.ReactNode;
}

export const NavBar = ({ children }: Props) => {
  const [open, setOpen] = useState(false);

  return (
    <div>
      <nav className="navbar navbar-expand-sm navbar-dark bg-dark mb-3">
        <div className="container">
          <a className="navbar-brand" href="#">
            MusicApp
          </a>
          <button
            className="navbar-toggler"
            type="button"
            onClick={() => setOpen(!open)}
          >
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className={cls("collapse navbar-collapse", open && "show")}>
            <ul className="navbar-nav">
              <li className="nav-item">
                {/* history.pushState('','','/playlists) */}
                <NavLink
                  className="nav-link"
                  to="/playlists"
                  activeClassName="placki active"
                >
                  Playlists
                </NavLink>
              </li>
              <li className="nav-item">
                <NavLink className="nav-link" to="/music/search">
                  Search
                </NavLink>
              </li>
            </ul>
            <div className="ms-auto">
              {children}
              <UserWidget />
            </div>
          </div>
        </div>
      </nav>
    </div>
  );
};
