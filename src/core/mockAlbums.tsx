import { AlbumView } from "./model/Album";

export const mockAlbums: AlbumView[] = [
  {
    id: "123",
    name: "Album 123",
    type: "album",
    images: [
      { width: 300, height: 300, url: "https://www.placecage.com/c/300/300" },
    ],
  },
  {
    id: "234",
    name: "Album 234",
    type: "album",
    images: [
      { width: 300, height: 300, url: "https://www.placecage.com/c/200/200" },
    ],
  },
  {
    id: "345",
    name: "Album 345",
    type: "album",
    images: [
      { width: 300, height: 300, url: "https://www.placecage.com/c/300/300" },
    ],
  },
  {
    id: "456",
    name: "Album 456",
    type: "album",
    images: [
      { width: 300, height: 300, url: "https://www.placecage.com/c/300/300" },
    ],
  },
];
